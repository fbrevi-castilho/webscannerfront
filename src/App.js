import React, { Component } from "react";
import apiInstance from "./api";
import Slider from "react-slick"
import { Chart } from "react-google-charts";

import "./main.css"


class App extends Component{
  constructor(props){
    super(props);

    this.state = {
      images: [],
      words: {},
      url: '',
      search: '',
      chartLoaded: false
    }

    this.updateInput = this.updateInput.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);

  }

updateInput(event){
  this.setState({search : event.target.value})
}

 async handleSubmit (event){   
   event.preventDefault();  
  const response = await apiInstance.get('api/pagescanner/get?url=' + this.state.search);
  this.setState({ images: response.data.images, words: response.data.words, url:response.data.url, chartLoaded: true}); 
}

getCharData()
{
  const chartData = [['Word', 'Ocurrences']];
  const wordList = this.state.words;
  for (let i = 0; i < wordList.length; i += 1) {
    chartData.push([wordList[i].value, wordList[i].ocurrences])
  }
  return chartData;
}

  render(){
    const imageList = this.state.images;    
    const url = this.state.url;
    const chartLoaded = this.state.chartLoaded;
    const settings = {
			dots: false,
			infinite: true,
			autoplay: false,
			speed: 500,
			autoplaySpeed: 3000,
			slidesToShow: 5,
			slidesToScroll: 5,
      arrows:true,            
		};        

    return(    
      <div>
      <div className="container-search">
        <h1>Web Scanner</h1>  
        <div className="search">
          <form className="hidden-xs" onSubmit={this.handleSubmit}>
            <input placeholder="https://www.example.com" onChange={this.updateInput} />
            <button type="submit">
              Submit                   
            </button>
          </form>
        </div>
      </div>
      <div className="container-slider">
          <Slider {...settings}>
          {imageList.map((image, index) => (
            <div className="image-img" key={index}>
                <img src={image} alt="" />
            </div>                    
          ))}
          </Slider>     
      </div>   
      <div className={chartLoaded ? "container-chart loaded" : "container-chart"}>
          <Chart
            width={'800px'}
            height={'500px'}
            chartType="Bar"
            loader={<div>Loading Chart</div>}
            data={this.getCharData()}        
            options={{
              // Material design options
              chart: {
                title: 'Scan Data ',
                subtitle: url,
              },
            }}
          />     
      </div> 
    </div>
    );
  }
}

export default App;
